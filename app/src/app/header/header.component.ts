import {AfterViewInit, Component, OnInit} from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        $('#frame').sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateMiddle: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,
        });
    }
}
