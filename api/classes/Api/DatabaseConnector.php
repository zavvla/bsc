<?php
declare(strict_types=1);
/**
 * Created by PhpStorm
 * User: vladis
 */

namespace Api;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * Class DatabaseConnector
 *
 * @package Api\Entity
 */
class DatabaseConnector
{

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private static $conn;

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \RuntimeException
     */
    public static function initConnection(): \Doctrine\DBAL\Connection
    {
        if (null === self::$conn) {
            $config = new Configuration();
            $request = Request::createFromGlobals();
            $yaml = Yaml::parseFile($request->server->get('DOCUMENT_ROOT') . '/../.config.yml');

            try {
                self::$conn = DriverManager::getConnection($yaml['db'], $config);
            } catch (DBALException $e) {
                trigger_error($e->getMessage());
                throw new RuntimeException('Error on connect to database');
            }
        }

        return self::$conn;
    }
}
