<?php
declare(strict_types=1);
/**
 *
 */

namespace Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utility\Log;
use function GuzzleHttp\json_encode as jsonEncode;

/**
 * Class Base
 *
 * @package Api
 */
class Base
{
    /**
     * @var array
     */
    private $route;

    /**
     * Base constructor.
     */
    public function __construct()
    {
        $this->route = \Symfony\Component\Yaml\Yaml::parseFile(
            __DIR__ . '/../../api.yml'
        );
    }

    /**
     * Выполнить метод API
     *
     * @return bool
     */
    public function exec(): bool
    {
        $request = Request::createFromGlobals();

        $log = Log::getLog('api');

        $httpMethod = $request->server->get('REQUEST_METHOD');

        switch ($httpMethod) {
            case 'POST':
                $method = '\Siler\Route\post';
                break;
            case 'PUT':
                $method = '\Siler\Route\put';
                break;
            case 'DELETE':
                $method = '\Siler\Route\delete';
                break;
            case 'OPTIONS':
                $method = '\Siler\Route\options';
                break;
            default:
                $method = '\Siler\Route\get';
        }

        $method('/{method}', function ($uri) use ($log, $request, $httpMethod) {
            $method = $this->route[$uri['method']];

            if ($method) {
                if (\in_array($httpMethod, $method['http_method'], true)) {
                    if (0 === strpos((string)$request->headers->get('Content-Type'), 'application/json')) {
                        $data = json_decode($request->getContent(), true);
                        $request->request->replace(\is_array($data) ? $data : []);
                    }

                    switch ($httpMethod) {
                        case 'POST':
                            $params = $request->request->all();
                            break;
                        case 'UPDATE':
                            $params = $request->request->all();
                            break;
                        default:
                            $params = $request->query->all();
                    }

                    $log->info('REQUEST', [
                        'uri'    => $uri,
                        'params' => $params,
                        'method' => $httpMethod,
                        'call'   => $method['class'] . '::' . $method['method']
                    ]);

                    $params = \array_intersect_key($params, \array_flip($method['params']));
                    $func = $method['method'];
                    $this->response($request, (new $method['class']())->$func($params));
                } else {
                    $log->info('REQUEST', [
                        'uri'    => $uri,
                        'method' => $httpMethod,
                        $method['class'] . '::' . $method['method']
                    ]);

                    $this->response($request, [
                        'result'  => 'error',
                        'message' => 'http method not allowed'
                    ], Response::HTTP_METHOD_NOT_ALLOWED);
                }
            } else {
                $log->info('REQUEST', [
                    'uri'    => $uri,
                    'method' => $httpMethod
                ]);
                $this->response($request, [
                    'result'  => 'error',
                    'message' => 'unknown method'
                ], Response::HTTP_NOT_FOUND);
            }
        });

        $this->response($request, [
            'result'  => 'error',
            'message' => 'invalid request'
        ], Response::HTTP_BAD_REQUEST);

        return true;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param array                                     $result
     * @param int|null                                  $status
     */
    private function response(Request $request, array $result, int $status = null): void
    {
        Log::getLog('api')->info('RESULT', [
            'http_code' => $status ?: Response::HTTP_OK,
            'result'    => $result
        ]);

        $response = new Response(jsonEncode($result, JSON_UNESCAPED_UNICODE), $status ?: Response::HTTP_OK, [
            'content-type' => 'application/json; charset=utf-8'
        ]);

        $response->prepare($request);
        $response->send();

        exit(0);
    }
}
