<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: vladis
 */

namespace Api\Entity;

use Api\DatabaseConnector;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Table;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Data
 *
 * @package Api\Entity
 */
class Data
{
    /**
     * User constructor.
     *
     * @param $token
     */
    public const APP_DATA_TABLE = 'app_data';
    /**
     * @var \Doctrine\DBAL\Connection Подключение к базе
     */
    private $conn;

    /**
     * @var mixed
     */
    private $config;

    public function __construct()
    {
        $this->conn = DatabaseConnector::initConnection();

        try {
            $this->createTokenTable();
        } catch (DBALException $e) {
            trigger_error($e->getMessage());
            throw new RuntimeException('db error on table create. ' . $e->getMessage());
        }

        $this->config = $this->config();
    }

    /**
     * Создать таблицу для приложения
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createTokenTable(): bool
    {
        $manager = $this->conn->getSchemaManager();

        if (!$manager->tablesExist([self::APP_DATA_TABLE])) {
            $deliveryServices = new Table(self::APP_DATA_TABLE);
            $deliveryServices->addColumn('key', 'string');
            $deliveryServices->addColumn('data', 'text');

            $manager->createTable($deliveryServices);
        }

        return true;
    }

    /**
     * @return mixed
     */
    private function config()
    {
        $request = Request::createFromGlobals();

        return Yaml::parseFile($request->server->get('DOCUMENT_ROOT') . '/../.config.yml');
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function saveBloodInfo(array $params): array
    {
        $data = [
            'save' => false
        ];

        if ($params['token'] === $this->config['token']) {
            $result = $this->conn->createQueryBuilder()
                ->from(self::APP_DATA_TABLE)
                ->select('data')
                ->where("`key` = 'blood_info'")
                ->execute()
                ->fetch();

            if ($result) {
                $this->conn->createQueryBuilder()
                    ->update(self::APP_DATA_TABLE)
                    ->set('data', ':message')
                    ->where('`key` = :key')
                    ->setParameters([
                        ':key'     => 'blood_info',
                        ':message' => \GuzzleHttp\json_encode($params['blood_info'])
                    ])->execute();
            } else {
                $this->conn->createQueryBuilder()
                    ->insert(self::APP_DATA_TABLE)
                    ->values([
                        '`key`'  => ':key',
                        '`data`' => ':message'
                    ])
                    ->setParameters([
                        'key'     => 'blood_info',
                        'message' => \GuzzleHttp\json_encode($params['blood_info'])
                    ])
                    ->execute();
            }

            $data = [
                'save' => true
            ];
        }

        return $data;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getBloodInfo(array $params): array
    {
        $data = [];

        $result = $this->conn->createQueryBuilder()
            ->from(self::APP_DATA_TABLE)
            ->select('data')
            ->where("`key` = 'blood_info'")
            ->execute()
            ->fetch();

        if ($result['data']) {
            $data = \GuzzleHttp\json_decode($result['data']);
        }

        return [
            'blood_info' => $data
        ];
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function setMessage(array $params): array
    {
        $data = [
            'save' => false
        ];

        if ($params['token'] === $this->config['token']) {
            $result = $this->conn->createQueryBuilder()
                ->from(self::APP_DATA_TABLE)
                ->select('data')
                ->where("`key` = 'message'")
                ->execute()
                ->fetch();

            if ($result) {
                $this->conn->createQueryBuilder()
                    ->update(self::APP_DATA_TABLE)
                    ->set('data', ':message')
                    ->where('`key` = :key')
                    ->setParameters([
                        ':key'     => 'message',
                        ':message' => $params['message']
                    ])->execute();
            } else {
                $this->conn->createQueryBuilder()
                    ->insert(self::APP_DATA_TABLE)
                    ->values([
                        '`key`'  => ':key',
                        '`data`' => ':message'
                    ])
                    ->setParameters([
                        'key'     => 'message',
                        'message' => $params['message']
                    ])
                    ->execute();
            }

            $data = [
                'save' => true
            ];
        }

        return $data;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getMessage(array $params): array
    {
        $data = '';

        $result = $this->conn->createQueryBuilder()
            ->from(self::APP_DATA_TABLE)
            ->select('data')
            ->where("`key` = 'message'")
            ->execute()
            ->fetch();

        if ($result['data']) {
            $data = $result['data'];
        }

        return [
            'message' => $data
        ];
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function sendMail(array $params): array
    {
        $result = [
            'send' => false
        ];

        if ($params['token'] === $this->config['token']) {
            $headers = [
                "To: {$params['name_to']} <{$params['mail_to']}>",
                'From: App Blood <' . $params['mail_from'] . '>'
            ];

            $result = [
                'send' => mail($params['mail_to'], $params['subject'], $params['message'], implode("\r\n", $headers))
            ];
        }

        return $result;
    }
}
