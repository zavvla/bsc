<?php
declare(strict_types=1);
/**
 *
 */

namespace Utility;

use Exception;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Registry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Log
 */
class Log
{
    /**
     * Получить логгер
     *
     * @param string $code
     *
     * @return Logger
     *
     */
    final public static function getLog(string $code = null): Logger
    {
        # Формат записи даты в лог
        $dateFormat = 'Y-m-d H:i:s';

        # Формат записи в лог
        $output = "[%datetime%][%channel%][%level_name%]: %message% %context% %extra%\n";

        # Форматтер строк логов
        $formatter = new LineFormatter($output, $dateFormat);

        $code = $code ?? 'general';

        $logger = new Logger($code);

        $level = self::getLogLevel();


        $stream = new RotatingFileHandler(self::getLogPath($code), 30, $level);
        $stream->setFormatter($formatter);
        $logger->pushHandler($stream);

        $logger->pushProcessor(self::filter());

        try {
            Registry::addLogger($logger);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }

        /**@var Logger $log */
        /** @noinspection PhpUndefinedMethodInspection */
        return Registry::$code();
    }

    /**
     * Получить уровень логирования
     *
     * @return int Идентификатор уровня логирования
     * @internal param string $code Код процесса
     *
     */
    private static function getLogLevel(): int
    {
        $request = Request::createFromGlobals();
        $yaml = Yaml::parseFile($request->server->get('DOCUMENT_ROOT') . '/../.config.yml');

        return 'debug' === $yaml['logger'] ? Logger::DEBUG : Logger::ERROR;
    }

    /**
     * Получить путь к логу
     *
     * @param string $code
     *
     * @return string Путь к логу
     */
    private static function getLogPath(string $code = null): string
    {
        return null === $code
            ? $_SERVER['DOCUMENT_ROOT'] . '/api/logs/app.log'
            : $_SERVER['DOCUMENT_ROOT'] . '/api/logs/' . $code . '.log';
    }

    /**
     * @return \Closure
     */
    private static function filter(): callable
    {
        $filters = [
            'login',
            'pass'
        ];

        return function ($record) use ($filters) {
            foreach ($filters as $filter) {
                if ($record['context'][$filter]) {
                    $record['context'][$filter] = '***';
                }

                if ($record['context']['params'][$filter]) {
                    $record['context']['params'][$filter] = '***';
                }

                if ($record['context']['result'][$filter]) {
                    $record['context']['result'][$filter] = '***';
                }
            }

            return $record;
        };
    }
}
