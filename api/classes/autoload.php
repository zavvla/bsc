<?php
declare(strict_types=1);
/**
 *
 */
require_once __DIR__ . '/Utility/Psr4Autoloader.php';

$loader = new Utility\Psr4Autoloader();
$loader->register();

# регистрация неймспейсов
$loader->addNamespace('Api', __DIR__ . '/Api');
$loader->addNamespace('Utility', __DIR__ . '/Utility');
