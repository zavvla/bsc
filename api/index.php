<?php
/**
 *
 */


if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
}

if (file_exists(__DIR__ . '/classes/autoload.php')) {
    require_once __DIR__ . '/classes/autoload.php';
}

try {
    $api = new Api\Base();
    $api->exec();
} catch (InvalidArgumentException $e) {
    trigger_error($e->getMessage());
    exit(\GuzzleHttp\json_encode([
        'result'  => 'error',
        'message' => $e->getMessage()
    ]));
} catch (Exception $e) {
    trigger_error($e->getMessage());
    exit(\GuzzleHttp\json_encode([
        'result'  => 'error',
        'message' => 'runtime exception',
        'text'    => $e->getMessage()
    ]));
} catch (Error $e) {
    trigger_error($e->getMessage());
    exit(\GuzzleHttp\json_encode([
        'result'  => 'error',
        'message' => 'runtime error',
        'text'    => $e->getMessage()
    ]));
}